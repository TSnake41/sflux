/*
  sflux_extra.c: Very-lightweight string stream library.
  Copyright (C) 2018 Teddy Astie

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef H_SFLUX_EXTRA
#define H_SFLUX_EXTRA

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>

#include "sflux.h"

int sflux_from_file(struct sflux *f, FILE *file);
int sflux_append(struct sflux *f, const void *buffer, size_t count);
int sflux_remove(struct sflux *f, size_t count, bool shrink);
int sflux_copy_bounds(struct sflux *f, struct sflux *copy, size_t offset,
  size_t count, bool shallow);
int sflux_append(struct sflux *f, const void *buffer, size_t count);
const char *sflux_chr(struct sflux *f, char c);
int sflux_vprintf(struct sflux *f, const char *format, va_list arg);
int sflux_vnprintf(struct sflux *f, size_t max_size, const char *format, va_list arg);
int sflux_printf(struct sflux *f, const char *format, ...);
int sflux_nprintf(struct sflux *f, size_t max_size, const char *format, ...);
int sflux_read_peek(struct sflux *f, void *buffer, size_t count);

#endif /* H_SFLUX_EXTRA */
