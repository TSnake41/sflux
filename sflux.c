/*
  sflux.c: Very-lightweight string stream library.
  Copyright (C) 2018 Teddy Astie

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "sflux.h"

int sflux_create_empty(struct sflux *f, size_t size)
{
  f->size = size;
  f->buffer = calloc(1, size);
  if (f->buffer == NULL)
    return SFLUX_OUT_OF_MEMORY;

  f->ptr = f->buffer;
  f->is_copy = false;
  return 0;
}

int sflux_from_buffer(struct sflux *f, void *buffer, size_t size, bool copy)
{
  if (copy) {
    if (sflux_create_empty(f, size))
      return SFLUX_OUT_OF_MEMORY;

    f->is_copy = false;
    memcpy(f->buffer, buffer, size);
  } else {
    f->buffer = buffer;
    f->ptr = f->buffer;
    f->size = size;
  }

  return 0;
}

void sflux_free(struct sflux *f)
{
  if (!f->is_copy) {
    free(f->buffer);
  }
}

int sflux_copy(const struct sflux *f, struct sflux *copy, bool shallow)
{
  if (shallow) {
    /* Do a shallow copy */
    copy->buffer = f->buffer;
    copy->ptr = f->ptr;
    copy->size = f->size;
    copy->is_copy = true;
  } else {
    /* Duplicate full stream. */
    if (sflux_create_empty(copy, f->size))
      return SFLUX_OUT_OF_MEMORY;

    memcpy(copy->buffer, f->buffer, f->size);
  }

  return 0;
}

int sflux_resize(struct sflux *f, size_t new_size)
{
  if (f->is_copy)
    /* We can't resize a copy. */
    return SFLUX_IS_COPY;

  size_t offset = sflux_offset(f);

  char *new_buffer = realloc(f->buffer, new_size);
  if (new_buffer == NULL)
    /* Can't resize */
    return SFLUX_OUT_OF_MEMORY;

  if (offset > new_size)
    /* Buffer is too small (new pointer will be outside bounds). */
    return SFLUX_OUT_OF_BOUNDS;

  f->buffer = new_buffer;
  f->ptr = f->buffer + offset;
  f->size = new_size;
  return 0;
}

size_t sflux_offset(struct sflux *f)
{
  return f->ptr - f->buffer;
}

int sflux_seek(struct sflux *f, size_t offset)
{
  if (offset > f->size)
    /* Offset lead to out of bounds data. */
    return SFLUX_OUT_OF_BOUNDS;

  f->ptr = f->buffer + offset;
  return 0;
}

int sflux_read(struct sflux *f, void *buffer, size_t count)
{
  size_t remain = f->size - sflux_offset(f);
  if (remain < count)
    return SFLUX_OUT_OF_BOUNDS;

  memcpy(buffer, f->ptr, count);
  f->ptr += count;
  return 0;
}

int sflux_write(struct sflux *f, const void *buffer, size_t count, bool resize)
{
  if (f->is_copy)
    return SFLUX_IS_COPY;

  size_t remain = f->size - sflux_offset(f);

  if (remain < count) {
    /* Buffer is too small, must resize. */
    if (!resize)
      /* Can't resize. */
      return SFLUX_TOO_SMALL;

    if (sflux_resize(f, f->ptr + count - f->buffer))
      /* Resize error (out of memory). */
      return SFLUX_OUT_OF_MEMORY;
  }

  memcpy(f->ptr, buffer, count);
  f->ptr += count;
  return 0;
}
