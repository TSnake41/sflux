/*
  sflux_extra.c: Very-lightweight string stream library.
  Copyright (C) 2018 Teddy Astie

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "sflux.h"

int sflux_from_file(struct sflux *f, FILE *file)
{
  if (sflux_create_empty(f, 0))
    return SFLUX_OUT_OF_MEMORY;

  void *buffer = malloc(8192);
  if (buffer == NULL)
    return SFLUX_OUT_OF_MEMORY;

  size_t count;
  do {
    count = fread(buffer, 1, 8192, file);
    if (ferror(file)) {
      free(buffer);
      return SFLUX_ERROR;
    }

    if (sflux_write(f, buffer, count, true)) {
      free(buffer);
      return SFLUX_OUT_OF_MEMORY;
    }
  } while(!feof(file));

  free(buffer);
  return 0;
}

int sflux_copy_partial(struct sflux *f, struct sflux *copy, size_t count, bool shallow)
{
  if ((sflux_offset(f) + count) > f->size)
    return SFLUX_OUT_OF_BOUNDS;

  if (shallow) {
    /* Do a shallow copy */
    copy->buffer = f->buffer;
    copy->ptr = f->ptr;
    copy->size = count;
    copy->is_copy = true;
  } else {
    /* Duplicate full stream. */
    if (sflux_create_empty(copy, count))
      return SFLUX_OUT_OF_MEMORY;

    memcpy(copy->buffer, f->buffer, count);
  }

  return 0;
}

int sflux_copy_bounds(struct sflux *f, struct sflux *copy, size_t offset,
  size_t count, bool shallow)
{
  if ((offset + count) > f->size)
    return SFLUX_OUT_OF_BOUNDS;

  if (shallow) {
    /* Do a shallow copy */
    copy->buffer = f->buffer + offset;
    copy->ptr = f->buffer + offset;
    copy->size = count;
    copy->is_copy = true;
  } else {
    /* Duplicate full stream. */
    if (sflux_create_empty(copy, count))
      return SFLUX_OUT_OF_MEMORY;

    f->ptr = f->buffer + offset;
    memcpy(copy->buffer + offset, f->buffer, count);
  }

  return 0;
}

int sflux_append(struct sflux *f, const void *buffer, size_t count)
{
  if (f->is_copy)
    return SFLUX_IS_COPY;

  size_t remain = f->size - sflux_offset(f);

  if (sflux_resize(f, f->size + count))
    return SFLUX_OUT_OF_MEMORY;

  memmove(f->ptr + count, f->ptr, remain);
  memcpy(f->ptr, buffer, count);
  return 0;
}

int sflux_remove(struct sflux *f, size_t count, bool shrink)
{
  if (f->is_copy)
    return SFLUX_IS_COPY;

  size_t remain = f->size - sflux_offset(f);
  if (remain > count)
    count = remain;

  memmove(f->ptr, f->ptr + count, remain - count);
  if (shrink) {
    if (sflux_resize(f, f->size - count))
      return SFLUX_OUT_OF_MEMORY;
  }
  return 0;
}

const char *sflux_chr(struct sflux *f, char c)
{
  size_t max_count = f->size - sflux_offset(f);
  const char *ptr = f->ptr;

  while (max_count--) {
    if (*ptr == c)
      return ptr;
    ptr++;
  }

  return NULL;
}

int sflux_vprintf(struct sflux *f, const char *format, va_list arg)
{
  if (f->is_copy) {
    return SFLUX_IS_COPY;
  }

  size_t count = vsnprintf(NULL, 0, format, arg);

  size_t remain = f->size - sflux_offset(f);
  if (remain < count) {
    /* Buffer is too small, must resize. */
    if (sflux_resize(f, f->ptr + count - f->buffer))
      /* Resize error (out of memory). */
      return SFLUX_OUT_OF_MEMORY;
  }

  vsprintf(f->ptr, format, arg);
  return 0;
}

int sflux_vnprintf(struct sflux *f, size_t max_size, const char *format, va_list arg)
{
  if (f->is_copy) {
    return SFLUX_IS_COPY;
  }

  size_t count = vsnprintf(NULL, 0, format, arg);

  size_t remain = f->size - sflux_offset(f);
  if (remain < count) {
    /* Buffer is too small, must resize. */
    if (sflux_resize(f, f->ptr + count - f->buffer))
      /* Resize error (out of memory). */
      return SFLUX_OUT_OF_MEMORY;
  }

  vsnprintf(f->ptr, max_size, format, arg);
  return 0;
}

int sflux_printf(struct sflux *f, const char *format, ...)
{
  va_list arg;
  va_start(arg, format);
  int status = sflux_vprintf(f, format, arg);
  va_end(arg);

  return status;
}

int sflux_nprintf(struct sflux *f, size_t max_size, const char *format, ...)
{
  va_list arg;
  va_start(arg, format);
  int status = sflux_vnprintf(f, max_size, format, arg);
  va_end(arg);

  return status;
}

int sflux_read_peek(struct sflux *f, void *buffer, size_t count)
{
  int status = sflux_read(f, buffer, count);
  if (status)
    return status;

  f->ptr -= count;
  return 0;
}
