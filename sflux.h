/*
  sflux.c: Very-lightweight string stream library.
  Copyright (C) 2018 Teddy Astie

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef H_SFLUX
#define H_SFLUX

#include <stdlib.h>
#include <stdbool.h>

struct sflux {
  size_t size;
  char *ptr;
  char *buffer;
  bool is_copy;
};

enum sflux_error {
  SFLUX_SUCCESS = 0,
  SFLUX_OUT_OF_MEMORY,
  SFLUX_OUT_OF_BOUNDS,
  SFLUX_IS_COPY,
  SFLUX_TOO_SMALL,
  SFLUX_ERROR
};

int sflux_create_empty(struct sflux *f, size_t size);
int sflux_from_buffer(struct sflux *f, void *buffer, size_t size, bool copy);
void sflux_free(struct sflux *f);
int sflux_copy(const struct sflux *f, struct sflux *copy, bool shallow);
int sflux_resize(struct sflux *f, size_t new_size);
size_t sflux_offset(struct sflux *f);
int sflux_seek(struct sflux *f, size_t offset);
int sflux_read(struct sflux *f, void *buffer, size_t count);
int sflux_write(struct sflux *f, const void *buffer, size_t count, bool resize);

#endif /* H_SFLUX */
